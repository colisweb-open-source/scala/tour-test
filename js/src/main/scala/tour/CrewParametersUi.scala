package tour

import com.raquo.laminar.api.L._
import tour.UiInputs.inputNumberWithLabel
import tour.domain.CrewParameters
import io.laminext.syntax.tailwind._

final case class CrewParametersUi($parameters: Var[CrewParameters]) {
  val kmCost: Signal[String] =
    $parameters.signal.map(_.kmCost.formatted("%.2f"))
  val baseHourCost
      : Signal[String] = //FIXME: this cost is not taken into account
    $parameters.signal.map(_.baseHourCost.formatted("%.2f"))
  val extraHourCost: Signal[String] =
    $parameters.signal.map(_.extraHourCost.formatted("%.2f"))
  val maxExtraHours: Signal[String] =
    $parameters.signal.map(_.maxExtraHours.formatted("%.1f"))
  val speed: Signal[String] =
    $parameters.signal.map(_.speedKmPerHour.formatted("%.0f"))
  val includedHours: Signal[String] =
    $parameters.signal.map(_.includedHours.formatted("%.2f"))
  val gCO2Vehicle: Signal[String] =
    $parameters.signal.map(_.emissionRategCO2PerKm.formatted("%.1f"))

  val render: HtmlElement =
    div(
      div.card.title("Configuration équipage"),
      div.card.body(
        cls := "grid-flow-row",
        inputNumberWithLabel($parameters)(
          "coutKm",
          "Coût €/km",
          0.01,
          kmCost,
          (p, v) => p.copy(kmCost = v.toDouble)
        ),
        inputNumberWithLabel($parameters)(
          "speed",
          "Vitesse km/h",
          1,
          speed,
          (p, v) => p.copy(speedKmPerHour = v.toDouble)
        ),
        inputNumberWithLabel($parameters)(
          "includedHours",
          "Heures incluses",
          0.5,
          includedHours,
          (p, v) => p.copy(includedHours = v.toDouble)
        ),
        inputNumberWithLabel($parameters)(
          "baseHourCost",
          "Cout de base (€/h)",
          0.5,
          baseHourCost,
          (p, v) => p.copy(baseHourCost = v.toDouble)
        ),
        inputNumberWithLabel($parameters)(
          "extraHourCost",
          "Coût € / heure supplémentaire",
          0.1,
          extraHourCost,
          (p, v) => p.copy(extraHourCost = v.toDouble)
        ),
        inputNumberWithLabel($parameters)(
          "maxExtraHours",
          "Nb d'heures sup maximum",
          0.1,
          maxExtraHours,
          (p, v) => p.copy(maxExtraHours = v.toDouble)
        ),
        inputNumberWithLabel($parameters)(
          "gCO2",
          "Emissions du véhicule (gCO2 / km)",
          0.1,
          gCO2Vehicle,
          (p, v) => p.copy(emissionRategCO2PerKm = v.toDouble)
        )
      )
    )
}
