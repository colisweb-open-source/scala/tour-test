package tour

import com.raquo.laminar.api.L._
import tour.UiInputs.inputNumberWithLabel
import tour.domain.{Coordinates, Delivery}

final case class DeliveryUi($delivery: Var[Delivery]) {

  private val price = $delivery.signal.map(_.sellPrice.formatted("%.2f"))
  private val id    = $delivery.signal.map(_.orderId)

  val render: HtmlElement =
    span(
      onMountInsert { implicit ctx =>
        span(
          inputNumberWithLabel($delivery)(
            "id",
            "Numéro de commande",
            1,
            id,
            (d, p) => d.copy(orderId = p)
          ),
          inputNumberWithLabel($delivery)(
            "price",
            "Prix de vente (€)",
            0.01,
            price,
            (d, p) => d.copy(sellPrice = p.toDouble)
          )
        )
      },
      onMountInsert { implicit ctx =>
        AddressUi(zoomForCoordinates).render
      }
    )

  def zoomForPrice(implicit context: MountContext[_]): Var[Double] =
    $delivery.zoom(_.sellPrice)(d => $delivery.now().copy(sellPrice = d))(
      context.owner
    )

  def zoomForCoordinates(implicit context: MountContext[_]): Var[Coordinates] =
    $delivery.zoom(_.coordinates)(d => $delivery.now().copy(coordinates = d))(
      context.owner
    )
}
