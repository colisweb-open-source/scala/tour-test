package tour.domain

final case class CrewParameters(
    kmCost: Double = 1.8,
    speedKmPerHour: Double = 30,
    extraHourCost: Double = 25,
    baseHourCost: Double = 25,
    includedHours: Double = 7,
    maxExtraHours: Double = 2,
    emissionRategCO2PerKm: Double = 225
)
